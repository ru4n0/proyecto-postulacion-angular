import { Component, OnInit } from '@angular/core';
import { Test1Service, IRemoteResponse } from '../../servicios/test1.service';

@Component({
  selector: 'app-test1',
  templateUrl: './test1.component.html',
  styleUrls: ['./test1.component.css']
})
export class Test1Component implements OnInit {

  datos_mostrar = [];
  datos_ordenados = [];
  cargando = false;
  constructor( private _Test1Service: Test1Service) { }

  ngOnInit() {
  
  }

  obtenerDataTest1(){
    this.cargando = true
	this._Test1Service.getData().subscribe( resp =>{
		resp.data = ((resp.data === null) ? [] : resp.data);
  		let procesado = this.procesar_respuesta(resp.data);
  		this.datos_mostrar = procesado.respuesta;
  		this.datos_ordenados = this.ordenar_numeros(procesado.datos_ordenados).join(", ");
  		this.cargando = false;
  	})
  }

  procesar_respuesta(data){
  	let resp = [];
  	let valor = {};
  	let datos_ordenados = []
	const l = data.length
  	
  	for(let i = 0; i < l; i++){
  		var posicion = this.buscar_en_arreglo(resp, data[i]);

  		if(posicion > -1){
  			resp[posicion].last_position = i;
  			resp[posicion].quantity +=1;
  		}
  		else{
  			let temp = {
  				'numero' : data[i],
  				'first_position' : i,
  				'last_position' : i,
  				'quantity' : 1
  			}
  			datos_ordenados.push(data[i])
  			resp.push(temp);

  		}
  	}

  	return {'respuesta': resp, 'datos_ordenados': datos_ordenados};
  }

  private buscar_en_arreglo(data, valor){
	const l = data.length;
	let resp = -1;

  	for(let i=0; i < l; i++){
  		if(data[i].numero == valor){
  			resp = i;
  		}
  	}
  	return resp;
  }


  private ordenar_numeros(data){
  	if(data!=null){
  	  const l = data.length
  	  for(let i=0; i<l; i++){
  	  	for(let j=0; j < l; j++){
  	  		if ( data[ j ] > data[ j + 1 ] ) {
	        	[ data[ j ], data[ j + 1 ] ] = [ data[ j + 1 ], data[ j ] ];
	      	}
  	  	}
  	  }
  	}
  	return data;
  }

}
