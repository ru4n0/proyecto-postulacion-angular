import { Component, OnInit } from '@angular/core';
import { Test2Service, IRemoteResponse } from '../../servicios/test2.service';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrls: ['./test2.component.css']
})
export class Test2Component implements OnInit {

  abecedario = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","U","V","W","X","Y","Z"];

  

  datos = [];
  cargando = false;

  constructor( private _Test2Service: Test2Service) { }

  ngOnInit() {
  }

  obtenerDataTest2(){
  	this.cargando = true;
	  this._Test2Service.getData().subscribe( resp =>{
		let data_procesada = this.procesar_informacion(resp.data)
		this.datos = data_procesada
		this.cargando = false;
	})}

  private procesar_informacion(data){
    let resp = []
    const l1 = this.abecedario.length;
    const l2 = data.length;

  
	for(let j = 0; j < l2; j++){ //leo data			l2
		let temp = {'suma':0, 'paragraph': ''}
		temp.suma = 0
    	for(let i = 0; i < l1; i++){ //leo letras
    		temp[this.abecedario[i]] = 0;
    		let valor = data[j].paragraph;
    		temp.suma = 0;
    		let repeticiones = 0;

    		for(let k = 0; k < valor.length; k++){ //leo letras de paragraph
    			if(isNaN(parseInt(valor[k])) === false && i==l1-1){
    			  temp.suma += parseInt(valor[k]);
    			}
    			else{
    				if(valor[k].toUpperCase()===this.abecedario[i]){
    				  repeticiones++;
    				}
    			}
    		}

    		temp[this.abecedario[i]] = repeticiones;
    	}
    	resp.push(temp)
    }

    return resp;
  }
}
