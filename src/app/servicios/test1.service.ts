import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators'


@Injectable({
  providedIn: 'root'
})

export class Test1Service {
  private url = 'http://168.232.165.184/prueba/array';
  constructor(private http: HttpClient) { 
  }

  getData(){
	return this.http.get(`${this.url}`)//.pipe(
  }
}

export interface IRemoteResponse{
	data?:any;
	error?:any;
	success:boolean;
}