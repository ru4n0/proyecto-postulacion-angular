import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

//rutas
import { APP_ROUTING } from './app.routes'


//servicios
import { Test1Service } from './servicios/test1.service';
import { Test2Service } from './servicios/test2.service';

//Componentes
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { BodyComponent } from './components/body/body.component';
import { HomeComponent } from './components/home/home.component';
import { Test2Component } from './components/test2/test2.component';
import { Test1Component } from './components/test1/test1.component';
import { AboutComponent } from './components/about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    BodyComponent,
    HomeComponent,
    Test2Component,
    Test1Component,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    HttpClientModule
  ],
  providers: [
  Test1Service,
  Test2Service],
  bootstrap: [AppComponent]
})
export class AppModule { }
