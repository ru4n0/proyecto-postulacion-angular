import { RouterModule, Routes } from '@angular/router';
import { HomeComponent} from './components/home/home.component';
import { Test1Component} from './components/test1/test1.component';
import { Test2Component} from './components/test2/test2.component';
import { AboutComponent} from './components/about/about.component';


const APP_ROUTES:Routes = [
	{path:'home', component: HomeComponent},
	{path:'test1', component: Test1Component},
	{path:'test2', component: Test2Component},
	{path:'about', component: AboutComponent},
	{path:'**', pathMatch: 'full', redirectTo: 'home'},
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);